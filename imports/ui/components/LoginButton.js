import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Meteor} from "meteor/meteor"

class LoginButton extends Component {

    render() {
        const style = {
            backgroundColor: this.props.exactPath === "/login" ?
                "#adb5bd" :
                this.props.user ?
                    "#dc3545" :
                    "#28a745",
            animation: this.props.loginButtonAnimation
        }

        const setLoginHandler = !this.props.animating ?
            this.props.exactPath === "/login" ?
                () => {
                    this.props.previousPath ?
                        history.back() :
                        this.props.actions.setPagePath("/", true)
                } :
                () => {
                    this.props.user ?
                        Meteor.logout() :
                        this.props.actions.setPagePath("/login", true)
                } :
            null

        return (
            <div
                style={style}
                onClick={setLoginHandler}
                onAnimationEnd={() => this.props.actions.setLoginButtonAnimation(null)}
                className="login-button"
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        animating: state.animating,
        exactPath: state.exactPath,
        loginButtonAnimating: state.loginButtonAnimating,
        previousPath: state.previousPath,
        user: state.user
    }
}

export default connect(mapStateToProps)(LoginButton)